package net.crytec.ctconomy.listener;

import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerPreLoginEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import net.crytec.ctconomy.CryConomy;
import net.crytec.ctconomy.utils.UUIDFetcher;

public class EconomyListener implements Listener {

	private CryConomy plugin;

	public EconomyListener(CryConomy plugin) {
		this.plugin = plugin;
	}

	@EventHandler(priority = EventPriority.MONITOR)
	public void saveOnQuit(PlayerQuitEvent event) {
		plugin.getAccountManager().saveToDatabase(event.getPlayer().getUniqueId(), done -> plugin.getAccountManager().removeFromCache(event.getPlayer().getUniqueId()));
	}

	@EventHandler
	public void sendWelcome(PlayerJoinEvent event) {
		UUIDFetcher.addToCache(event.getPlayer());
	}

	@EventHandler(priority = EventPriority.MONITOR)
	public void preLogin(AsyncPlayerPreLoginEvent e) {
		if (plugin.getAccountManager().hasAccount(e.getUniqueId())) {
			plugin.getAccountManager().getAccount(e.getUniqueId());
		} else {
			plugin.getAccountManager().createNewAccount(e.getUniqueId());
		}
	}
}
