package net.crytec.ctconomy;

import java.util.HashSet;
import java.util.UUID;

import org.bukkit.Bukkit;

public class AutoSave implements Runnable {

	@Override
	public void run() {
		CryConomy.getInstance().getAccountManager().saveAll();
		HashSet<UUID> quenue = new HashSet<UUID>();

		for (UUID uuid : CryConomy.getInstance().getAccountManager().getAccounts().keySet()) {
			if (Bukkit.getPlayer(uuid) == null) {
				quenue.add(uuid);
			}
		}

		int i = 0;
		for (UUID uuid : quenue) {
			CryConomy.getInstance().getAccountManager().removeFromCache(uuid);
			i++;
		}
		if (i > 0) {
			CryConomy.getInstance().getLogger().info("Removed " + i + " players from economy cache.");
		}
	}
}