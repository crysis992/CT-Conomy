package net.crytec.ctconomy.commands;

import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import co.aikar.commands.BaseCommand;
import co.aikar.commands.CommandIssuer;
import co.aikar.commands.annotation.CommandAlias;
import co.aikar.commands.annotation.CommandCompletion;
import co.aikar.commands.annotation.CommandPermission;
import co.aikar.commands.annotation.Default;
import co.aikar.commands.annotation.Description;
import co.aikar.commands.annotation.Optional;
import co.aikar.commands.annotation.Subcommand;
import co.aikar.commands.bukkit.contexts.OnlinePlayer;
import net.crytec.ctconomy.CryConomy;
import net.crytec.ctconomy.utils.Language;
import net.crytec.ctconomy.utils.Utils;

@CommandAlias("%rootCommand")
public class MoneyCommand extends BaseCommand {
	
	private CryConomy plugin;
	private String currency;
	
	public MoneyCommand(CryConomy plugin) {
		this.plugin = plugin;
		currency = plugin.getEcon().currencyNamePlural();
	}
	
    @Default
    @Description("Zeigt dir deinen aktuellen Kontostand.")
    @CommandCompletion("@players @nothing")
    public void onMoney(CommandSender sender, @Optional OfflinePlayer target) {
    	
    	if (target == null && sender instanceof Player) {
    		String bal = String.valueOf(Utils.trim(2, plugin.getEcon().getBalance( (Player) sender )));
    		sender.sendMessage(Language.MONEY_COMMAND.toChatString().replace("%money%", bal).replace("%currency%", currency));
    	} else if (target != null) {
    		String bal = String.valueOf(Utils.trim(2, plugin.getEcon().getBalance(target)));
    		sender.sendMessage(Language.MONEY_COMMAND_OTHER.toChatString().replace("%money%", bal).replace("%currency%", currency).replace("%target%", target.getName()));
    	}
    }
        
    @Subcommand("pay|send")
    @Description("Sendet Geld an einen anderen Spieler.")
    @CommandCompletion("@players @nothing")
    public void onPay(Player issuer, OnlinePlayer spieler, double geld) {
    	
    	if (spieler.getPlayer().getUniqueId().equals(issuer.getUniqueId())) {
    		issuer.sendMessage(Language.ERROR_SELF.toChatString());
    		return;
    	}
    	
    	if (geld <= 0) return;
    	
		if (CryConomy.getInstance().getAccountManager().withdrawPlayer(issuer.getUniqueId(), geld)) {
			CryConomy.getInstance().getAccountManager().depositPlayer(spieler.getPlayer().getUniqueId(), geld);
			issuer.sendMessage(Language.MONEY_COMMAND_PAY_SELF.toChatString().replace("%target%", spieler.getPlayer().getDisplayName()).replace("%money%", String.valueOf(geld)).replace("%currency%", plugin.getEcon().currencyNameSingular()));
			spieler.getPlayer().sendMessage(Language.MONEY_COMMAND_PAY_TARGET.toChatString().replace("%target%", issuer.getDisplayName()).replace("%money%", String.valueOf(geld)).replace("%currency%", plugin.getEcon().currencyNameSingular()));
		} else {
			issuer.sendMessage(Language.ERROR_NOT_ENOUGH_MONEY.toChatString());
			return;
		}
    }
    
	@Subcommand("top")
	@Description("Zeigt die Top 10 Liste der Spieler.")
	public void onPay(CommandIssuer sender) {

		plugin.getAccountManager().getTop10(top -> {
			sender.sendMessage(Language.MONEY_TOP_10_HEAD.toChatString());
			for (int i = 0; i < top.size(); i++) {
				sender.sendMessage(top.get(i));
			}
			sender.sendMessage("");
		});
	}
    
    
    @Subcommand("set")
    @CommandPermission("ctc.admin.set")
    @Description("Setzt das Geld von einem Spieler auf den angegebenen Wert.")
    public void onAdminSet(CommandSender player, OfflinePlayer op, double amount) {
		plugin.getAccountManager().getAccount(op.getUniqueId()).setBalance(amount);
		player.sendMessage(Language.MONEY_COMMAND_SET_SENDER.toChatString().replace("%money%", String.valueOf(amount)).replace("%currency%", currency).replace("%target%", String.valueOf(op.getName())));

		if (op.isOnline()) {
			op.getPlayer().sendMessage(Language.MONEY_COMMAND_SET_RECEIVER.toChatString().replace("%money%", String.valueOf(amount)).replace("%currency%", currency).replace("%target%", player.getName()));
		}
    }
    
    @Subcommand("give|add|grant")
    @CommandPermission("ctc.admin.give")
    @Description("Fügt dem angegebenen Spieler einen Betrag hinzu.")
    public void onAdminGive(CommandSender player, OfflinePlayer op, double amount) {
		plugin.getAccountManager().getAccount(op.getUniqueId()).deposit(amount);
		player.sendMessage(Language.MONEY_COMMAND_GIVE_SENDER.toChatString().replace("%target%", op.getName()).replace("%money%", String.valueOf(amount)).replace("%currency%", currency));
    	
    }
    
    @Subcommand("giveall|addall|grantall")
    @CommandPermission("ctc.admin.giveall")
    @Description("Fügt allen Spieler einen Betrag hinzu.")
	public void onAdminGiveAll(CommandSender sender, double amount) {
		for (Player player : Bukkit.getOnlinePlayers()) {
			plugin.getAccountManager().getAccount(player.getUniqueId()).deposit(amount);
		}

		plugin.getAccountManager().giveAllPlayers(amount);
		sender.sendMessage(Language.MONEY_COMMAND_GIVE_ALL.toChatString().replace("%money%", String.valueOf(amount)));
	}
    
    @Subcommand("take|withdraw")
    @CommandPermission("ctc.admin.take")
    @Description("Zieht dem angegebenen Spieler einen Betrag ab.")
    public void onAdminTake(CommandSender player, OfflinePlayer op, double amount) {
		plugin.getAccountManager().getAccount(op.getUniqueId()).deposit(amount);
		player.sendMessage(Language.MONEY_COMMAND_TAKE_SENDER.toChatString().replace("%target%", op.getName()).replace("%money%", String.valueOf(amount)).replace("%currency%", currency));
    	
    }
}