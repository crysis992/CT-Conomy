package net.crytec.ctconomy.storage;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.function.Consumer;

import org.apache.commons.lang.Validate;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;

import net.crytec.ctconomy.CryConomy;
import net.crytec.ctconomy.eco.EcoAccount;
import net.crytec.ctconomy.utils.Language;
import net.crytec.ctconomy.utils.Utils;

public class MySQL implements IStorage {

	private CryConomy plugin;

	private Map<UUID, EcoAccount> accounts = new HashMap<UUID, EcoAccount>();
	private static String MYSQL_TABLE;
	private final ConnectionPoolManager pool;

	public MySQL(CryConomy instance) {
		this.plugin = instance;
		MySQL.MYSQL_TABLE = plugin.getConfig().getString("mysql.table");
		pool = new ConnectionPoolManager(plugin);
	}

	public ConnectionPoolManager getPool() {
		return this.pool;
	}

	public Map<UUID, EcoAccount> getAccounts() {
		return this.accounts;
	}

	public boolean hasAccount(UUID uuid) {
		Connection conn = null;
		ResultSet res = null;
		String sql = "SELECT uuid FROM " + MYSQL_TABLE + " WHERE uuid = '" + uuid.toString() + "'";

		try {
			conn = pool.getConnection();
			res = conn.createStatement().executeQuery(sql);
			return res.next();
		} catch (SQLException ex) {
			ex.printStackTrace();
			return false;
		} finally {
			pool.close(conn, null, res);
		}
	}
	
	public void giveAllPlayers(double amount) {
		Connection conn = null;

		String sql = "UPDATE " + MYSQL_TABLE + " SET balance = balance + '" + amount + "';";

		try {
			conn = pool.getConnection();
			conn.createStatement().executeUpdate(sql);
		} catch (SQLException ex) {
			ex.printStackTrace();
		} finally {
			pool.close(conn, null, null);
		}
	}

	public void createNewAccount(UUID uuid) {
		Connection conn = null;
		PreparedStatement ps = null;
		String sql = "INSERT INTO " + MYSQL_TABLE + " (uuid, balance, bank) VALUES (?, ?, ?);";

		try {
			conn = pool.getConnection();
			ps = conn.prepareStatement(sql);

			double bal = plugin.getConfig().getDouble("eco.defaultBalance");
			double bank = plugin.getConfig().getDouble("eco.defaultBankBalance");
			ps.setString(1, uuid.toString());
			ps.setDouble(2, bal);
			ps.setDouble(3, bank);
			ps.executeUpdate();
		} catch (SQLException ex) {
			ex.printStackTrace();
		} finally {
			pool.close(conn, ps, null);
		}
	}

	public void createNewAccount(UUID uuid, double bal, double bank) {
		Connection conn = null;
		PreparedStatement ps = null;
		String sql = "INSERT INTO " + MYSQL_TABLE + " (uuid, balance, bank) VALUES (?, ?, ?);";

		try {
			conn = pool.getConnection();
			ps = conn.prepareStatement(sql);

			ps.setString(1, uuid.toString());
			ps.setDouble(2, bal);
			ps.setDouble(3, bank);
			ps.executeUpdate();
		} catch (SQLException ex) {
			ex.printStackTrace();
		} finally {
			pool.close(conn, ps, null);
		}
	}

	public EcoAccount getAccount(UUID uuid) {
		Validate.notNull(uuid, "Cannot load a player without an UUID.");
		if (this.accounts.containsKey(uuid)) {
			return this.accounts.get(uuid);
		}
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet res = null;
		String sql = "SELECT * FROM " + MYSQL_TABLE + " WHERE uuid = ?";

		try {
			conn = pool.getConnection();
			ps = conn.prepareStatement(sql);
			ps.setString(1, uuid.toString());
			res = ps.executeQuery();

			if (res.next()) {
				EcoAccount acc = new EcoAccount(uuid, res.getDouble("balance"), res.getDouble("bank"));
				if (!this.accounts.containsKey(uuid)) {
					this.accounts.put(uuid, acc);
				}
				return acc;
			} else {
				return null;
			}

		} catch (SQLException ex) {
			ex.printStackTrace();
			return null;
		} finally {
			pool.close(conn, ps, res);
		}
	}

	public boolean withdrawPlayer(UUID uuid, double amount) {
		if (this.getAccount(uuid).getBalance() >= amount) {
			this.getAccount(uuid).setBalance(this.getAccount(uuid).getBalance() - amount);
			return true;
		} else {
			return false;
		}
	}

	public void depositPlayer(UUID uuid, double amount) {
		this.getAccount(uuid).setBalance(this.getAccount(uuid).getBalance() + amount);
	}

	public void saveToDatabase(UUID uuid) {
		this.saveToDatabase(uuid, done -> {
		});
	}

	public void saveAll() {
		for (UUID uuid : accounts.keySet()) {
			this.saveToDatabase(uuid);
		}
	}

	public void saveToDatabase(UUID uuid, Consumer<Boolean> done) {
		EcoAccount acc = this.getAccount(uuid);

		double balance = Utils.trim(2, acc.getBalance());
		double bank = Utils.trim(2, acc.getBank());

		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet res = null;
		String sql = "UPDATE " + MYSQL_TABLE + " SET balance = ? , bank = ? WHERE uuid = ?";

		try {
			conn = pool.getConnection();
			ps = conn.prepareStatement(sql);

			ps.setDouble(1, balance);
			ps.setDouble(2, bank);
			ps.setString(3, uuid.toString());
			ps.executeUpdate();
			done.accept(true);
		} catch (SQLException ex) {
			ex.printStackTrace();
			done.accept(false);
		} finally {
			pool.close(conn, ps, res);
		}
	}

	public void removeFromCache(UUID uuid) {
		if (this.accounts.containsKey(uuid)) {
			this.saveToDatabase(uuid);
			this.accounts.remove(uuid);
		}
	}

	@Override
	public void getTop10(Consumer<List<String>> top10) {
		ArrayList<String> top = new ArrayList<String>();

		Connection conn = null;
		ResultSet res = null;
		PreparedStatement ps = null;

		String sql = "SELECT uuid, balance as total FROM economy ORDER by total DESC LIMIT 10;";

		if (plugin.isBankEnabled()) {
			sql = "SELECT uuid, bank + balance as total FROM economy ORDER by total DESC LIMIT 10;";
		}
		try {
			conn = pool.getConnection();
			ps = conn.prepareStatement(sql);
			res = ps.executeQuery();

			int x = 1;
			while (res.next()) {
				double total = Utils.trim(2, res.getDouble("total"));
				UUID uuid = UUID.fromString(res.getString("uuid"));
				OfflinePlayer op = Bukkit.getOfflinePlayer(uuid);
				
				if (!op.hasPlayedBefore()) continue;
				
				top.add(Language.MONEY_TOP_10_ENTRY.toString().replace("%rank%", String.valueOf(x)).replace("%player%", op.getName()).replace("%money%", String.valueOf(total)).replace("%currency%", plugin.getEcon().currencyNamePlural()));
				x++;
			}
			top10.accept(top);
		} catch (SQLException ex) {
			ex.printStackTrace();
			top10.accept(Arrays.asList("Something went wrong!"));
		} finally {
			pool.close(conn, ps, res);
		}
	}

	@Override
	public void closeConnection() {
		this.getPool().closePool();
	}
}