package net.crytec.ctconomy.storage;

import net.crytec.ctconomy.CryConomy;

public class StorageManager {
	
	public enum StorageType {
		FLATFILE,
		MYSQL;
	}
	
	private final IStorage storage;

	public StorageManager(CryConomy instance, StorageType type) {
		
		if (type == StorageType.MYSQL) {
			this.storage = new MySQL(instance);
		} else {
			this.storage = new Flatfile(instance);
		}
	}
	
	public IStorage getStorage() {
		return this.storage;
	}
	
}
