package net.crytec.ctconomy;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Locale;

import org.bukkit.Bukkit;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.plugin.ServicePriority;
import org.bukkit.plugin.ServicesManager;
import org.bukkit.plugin.java.JavaPlugin;

import co.aikar.commands.PaperCommandManager;
import net.crytec.ctconomy.commands.MoneyCommand;
import net.crytec.ctconomy.eco.EconomyHook;
import net.crytec.ctconomy.listener.EconomyListener;
import net.crytec.ctconomy.storage.IStorage;
import net.crytec.ctconomy.storage.StorageManager;
import net.crytec.ctconomy.storage.StorageManager.StorageType;
import net.crytec.ctconomy.utils.Language;
import net.milkbowl.vault.Vault;
import net.milkbowl.vault.economy.Economy;

public class CryConomy extends JavaPlugin {

	private static CryConomy instance;

	private EconomyHook econHook;
	private ServicesManager serviceManager;
	private StorageManager manager;

	@Override
	public void onLoad() {
		CryConomy.instance = this;
		getServer().getServicesManager().register(Economy.class, this.econHook, (Vault) Bukkit.getPluginManager().getPlugin("Vault"), ServicePriority.Normal);
		
		if (!getDataFolder().exists()) {
			getDataFolder().mkdir();
		}
	}

	@Override
	public void onEnable() {
		
		this.updateConfiguration(new File(this.getDataFolder(), "config.yml"), this.getResource("config.yml"));
		this.updateConfiguration(new File(this.getDataFolder(), "language.yml"), this.getResource("language.yml"));
		
		this.loadLang();
		if (getConfig().getBoolean("mysql.enabled")) {
		this.manager = new StorageManager(this, StorageType.MYSQL);
		} else {
			this.manager = new StorageManager(this, StorageType.FLATFILE);
		}

		econHook = new EconomyHook(this);
		serviceManager = getServer().getServicesManager();

		Bukkit.getPluginManager().registerEvents(new EconomyListener(this), this);
				
		PaperCommandManager manager = new PaperCommandManager(this);
		manager.getCommandReplacements().addReplacement("rootCommand", this.getConfig().getString("commandalias", "goldmarken"));
		manager.registerCommand(new MoneyCommand(this));
		
		try {
			manager.getLocales().loadYamlLanguageFile(new File(this.getDataFolder(), "language.yml"), Locale.GERMAN);
		} catch (IOException | InvalidConfigurationException e) { e.printStackTrace(); }
		
		
		manager.getLocales().setDefaultLocale(Locale.GERMAN);

		for (Player p : Bukkit.getOnlinePlayers()) {
			if (getAccountManager().hasAccount(p.getUniqueId())) {
				getAccountManager().getAccount(p.getUniqueId());
			} else {
				getAccountManager().createNewAccount(p.getUniqueId());
			}
		}
		
		serviceManager.register(Economy.class, this.econHook, (Vault) Bukkit.getPluginManager().getPlugin("Vault"), ServicePriority.Normal);		
		Bukkit.getScheduler().runTaskTimerAsynchronously(this, new AutoSave(), 20 * 60 * 15, 20 * 60 * 15);
	}

	@Override
	public void onDisable() {
		if (serviceManager != null) {
			serviceManager.unregister(Economy.class, this.econHook);
			getAccountManager().saveAll();
			getAccountManager().closeConnection();
		}
	}

	private void updateConfiguration(File writtenConfig, InputStream configStream) {
		if (!writtenConfig.exists()) {
			this.saveResource(writtenConfig.getName(), true);
		} else {
			boolean save = false;
			YamlConfiguration config = YamlConfiguration.loadConfiguration(writtenConfig);
			YamlConfiguration defaultConfig = YamlConfiguration.loadConfiguration(new InputStreamReader(configStream));
			
			for (String key : defaultConfig.getKeys(true)) {
				if (defaultConfig.isConfigurationSection(key)) {
					if (config.getConfigurationSection(key) == null) {
						config.createSection(key);
						save = true;
					}
				} else {
					if (!config.isSet(key)) {
						config.set(key, defaultConfig.get(key));
						save = true;
					}
				}
			}
			if (save) {
				try {
					config.save(writtenConfig);
				} catch (Exception ex) {
					ex.printStackTrace();
				}
			}
		}
	}

	public IStorage getAccountManager() {
		return this.manager.getStorage();
	}

	public EconomyHook getEcon() {
		return this.econHook;
	}
	
	public boolean isBankEnabled() {
		return getConfig().getBoolean("eco.usebank", false);
	}

	public static CryConomy getInstance() {
		return CryConomy.instance;
	}
	
	public void loadLang() {
		File lang = new File(CryConomy.getInstance().getDataFolder(), "language.yml");
		if (!lang.exists()) {
			try {
				CryConomy.getInstance().getDataFolder().mkdir();
				lang.createNewFile();
				if (lang != null) {
					YamlConfiguration defConfig = YamlConfiguration.loadConfiguration(lang);
					defConfig.save(lang);
					Language.setFile(defConfig);
				}
			} catch (IOException e) {
				CryConomy.getInstance().getLogger().severe("Could not create language file!");
				Bukkit.getPluginManager().disablePlugin(CryConomy.getInstance());
			}
		}

		YamlConfiguration conf = YamlConfiguration.loadConfiguration(lang);
		for (Language item : Language.values()) {
			if (conf.getString(item.getPath()) == null) {
				if (item.isArray()) {
					conf.set(item.getPath(), item.getDefArray());
				} else {
					conf.set(item.getPath(), item.getDefault());
				}

			}
		}
		Language.setFile(conf);
		try {
			conf.save(lang);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}