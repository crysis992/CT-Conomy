package net.crytec.ctconomy.utils;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Random;

import org.bukkit.Location;
import org.bukkit.entity.Entity;
import org.bukkit.util.Vector;

public class Utils {
	public static double trim(int degree, double d) {
		String format = "#.#";

		for (int i = 1; i < degree; i++) {
			format = format + "#";
		}
		DecimalFormatSymbols dfs = DecimalFormatSymbols.getInstance();
		dfs.setDecimalSeparator('.');
		DecimalFormat twoDForm = new DecimalFormat(format, dfs);
		return Double.valueOf(twoDForm.format(d)).doubleValue();
	}

	public static Random random = new Random();

	public static int r(int i) {
		return random.nextInt(i);
	}

	public static double offset2d(Entity a, Entity b) {
		return offset2d(a.getLocation().toVector(), b.getLocation().toVector());
	}

	public static double offset2d(Location a, Location b) {
		return offset2d(a.toVector(), b.toVector());
	}

	public static double offset2d(Vector a, Vector b) {
		a.setY(0);
		b.setY(0);
		return a.subtract(b).length();
	}

	public static double offset(Entity a, Entity b) {
		return offset(a.getLocation().toVector(), b.getLocation().toVector());
	}

	public static double offset(Location a, Location b) {
		return offset(a.toVector(), b.toVector());
	}

	public static double offset(Vector a, Vector b) {
		return a.subtract(b).length();
	}

	public static boolean isDouble(String amount) {
		try {
			Double.parseDouble(amount);
			return true;
		} catch (NumberFormatException e) {
			return false;
		}
	}

	public static boolean isInt(String amount) {
		try {
			Integer.parseInt(amount);
			return true;
		} catch (NumberFormatException e) {
			return false;
		}
	}

	public static boolean isInCooldown(long lastUse, int cooldownInSec) {
		long secondsLeft = ((lastUse / 1000) + cooldownInSec) - (System.currentTimeMillis() / 1000);
		if (secondsLeft > 0) {
			return true;
		}
		return false;
	}

	/**
	 * Get a random number between {@link lower} and {@link upper} value
	 * 
	 * @param lower
	 * @param upper
	 * @return
	 */
	public static int getRandom(int lower, int upper) {
		return random.nextInt(upper - lower + 1) + lower;
	}

	/**
	 * Returns true if the given number is in the given range
	 * 
	 * @param x
	 *            Number to be checked.
	 * @param min
	 *            Minimum
	 * @param max
	 *            Maximum
	 * @return
	 */
	public static boolean betweenExclusive(int x, int min, int max) {
		return x > min && x < max;
	}

	/**
	 * Returns true if Integer {@link i} is divisible by {@link i}
	 * 
	 * @param by
	 * @param i
	 * @return
	 */
	public static boolean isDivisible(int by, int i) {
		if (i % by == 0) {
			return true;
		}
		return false;
	}
}
