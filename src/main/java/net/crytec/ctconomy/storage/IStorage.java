package net.crytec.ctconomy.storage;

import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.function.Consumer;

import net.crytec.ctconomy.eco.EcoAccount;

public interface IStorage {

	public Map<UUID, EcoAccount> getAccounts();
	public boolean hasAccount(UUID uuid);
	public void giveAllPlayers(double amount);
	public void createNewAccount(UUID uuid);
	public void createNewAccount(UUID uuid, double bal, double bank);
	public EcoAccount getAccount(UUID uuid);
	public boolean withdrawPlayer(UUID uuid, double amount);
	public void depositPlayer(UUID uuid, double amount);
	public void saveToDatabase(UUID uuid);
	public void saveAll();
	public void saveToDatabase(UUID uuid, Consumer<Boolean> done);
	public void removeFromCache(UUID uuid);
	public void getTop10(Consumer<List<String>> top);
	public void closeConnection();
	
}
