package net.crytec.ctconomy.storage;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.function.Consumer;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import net.crytec.ctconomy.CryConomy;
import net.crytec.ctconomy.eco.EcoAccount;

public class Flatfile implements IStorage {

	private Map<UUID, EcoAccount> accounts = new HashMap<UUID, EcoAccount>();
	private FileConfiguration config;
	private File config_file;
	private CryConomy plugin;
	
	
	public Flatfile(CryConomy instance) {
		this.plugin = instance;
		this.config_file = new File(instance.getDataFolder(), "accounts.yml");
		if (!config_file.exists()) {
			try {
				config_file.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		this.config = YamlConfiguration.loadConfiguration(config_file);
	}
	
	
	@Override
	public Map<UUID, EcoAccount> getAccounts() {
		return this.accounts;
	}

	@Override
	public boolean hasAccount(UUID uuid) {
		return config.isSet("accounts." + uuid.toString());
	}

	@Override
	public void giveAllPlayers(double amount) {
		CryConomy.getInstance().getLogger().severe("Giving money to all players is only supported in MySQL mode!");
	}

	@Override
	public void createNewAccount(UUID uuid) {
		double bal = plugin.getConfig().getDouble("eco.defaultBalance");
		double bank = plugin.getConfig().getDouble("eco.defaultBankBalance");
		
		config.set("accounts." + uuid.toString() + ".balance", bal);
		config.set("accounts." + uuid.toString() + ".bank", bank);
	}

	@Override
	public void createNewAccount(UUID uuid, double bal, double bank) {
		config.set("accounts." + uuid.toString() + ".balance", bal);
		config.set("accounts." + uuid.toString() + ".bank", bank);
	}

	@Override
	public EcoAccount getAccount(UUID uuid) {
		if (this.accounts.containsKey(uuid)) {
			return this.accounts.get(uuid);
		} else {
			EcoAccount account = new EcoAccount(uuid, config.getDouble("accounts." + uuid.toString() + ".balance"), config.getDouble("accounts." + uuid.toString() + ".bank"));
			this.accounts.put(uuid, account);
			return account;
		}
		
	}

	@Override
	public boolean withdrawPlayer(UUID uuid, double amount) {
		if (this.getAccount(uuid).getBalance() >= amount) {
			this.getAccount(uuid).setBalance(this.getAccount(uuid).getBalance() - amount);
			return true;
		} else {
			return false;
		}
	}

	@Override
	public void depositPlayer(UUID uuid, double amount) {
		this.getAccount(uuid).setBalance(this.getAccount(uuid).getBalance() + amount);
	}

	@Override
	public void saveToDatabase(UUID uuid) {
		config.set("accounts." + uuid.toString() + ".balance", getAccount(uuid).getBalance());
		config.set("accounts." + uuid.toString() + ".bank", getAccount(uuid).getBank());
	}

	@Override
	public void saveAll() {
		for (UUID uuid : accounts.keySet()) {
			this.saveToDatabase(uuid);
		}
		this.saveConfig();
	}

	@Override
	public void saveToDatabase(UUID uuid, Consumer<Boolean> done) {
		config.set("accounts." + uuid.toString() + ".balance", getAccount(uuid).getBalance());
		config.set("accounts." + uuid.toString() + ".bank", getAccount(uuid).getBank());
		done.accept(true);
		this.saveConfig();
	}

	@Override
	public void removeFromCache(UUID uuid) {
		if (this.accounts.containsKey(uuid)) {
			this.saveToDatabase(uuid);
			this.accounts.remove(uuid);
		}
	}


	@Override
	public void getTop10(Consumer<List<String>> top) {
		top.accept(Arrays.asList("§cTop 10 list is not supported in FlatFile Storage Mode!", "§cFlatfile should be only used for testing"));
	}
	
	private void saveConfig() {
		try {
			this.config.save(config_file);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}


	@Override
	public void closeConnection() {
		// Not required in FlatFile Mode
	}

}
