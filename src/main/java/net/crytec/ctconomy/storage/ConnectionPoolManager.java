package net.crytec.ctconomy.storage;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.bukkit.Bukkit;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import com.zaxxer.hikari.pool.HikariPool.PoolInitializationException;

import net.crytec.ctconomy.CryConomy;

public class ConnectionPoolManager {

	private HikariDataSource dataSource;

	private CryConomy plugin;

	public ConnectionPoolManager(CryConomy plugin) {
		this.plugin = plugin;
		this.setupPool();
	}

    private void setupPool() {
        HikariConfig config = new HikariConfig();
		config.setJdbcUrl("jdbc:mysql://" + plugin.getConfig().getString("mysql.host") + ":" + plugin.getConfig().getInt("mysql.port") + "/" + plugin.getConfig().getString("mysql.database"));
        config.setDriverClassName("com.mysql.jdbc.Driver");
        config.setUsername(plugin.getConfig().getString("mysql.user"));
        config.setPassword(plugin.getConfig().getString("mysql.password"));
        config.setMinimumIdle(5);
        config.setMaximumPoolSize(5);
        config.setConnectionTimeout(5000);
        config.setConnectionTestQuery("SELECT 1;");
        try {
        dataSource = new HikariDataSource(config);
        } catch (PoolInitializationException ex) {
        	Bukkit.getLogger().severe("[CTConomy] Failed to connect to the database! Cannot start the plugin!");
        	Bukkit.getPluginManager().disablePlugin(CryConomy.getInstance());
        	return;
        }
        
		try {
			Statement stmt = this.getConnection().createStatement();

			stmt.addBatch("CREATE TABLE IF NOT EXISTS `" + plugin.getConfig().getString("mysql.table")
					+ "` (`uuid`  varchar(36) NULL ,`balance`  double NULL DEFAULT 0 ,`bank`  double NULL DEFAULT 0 ,PRIMARY KEY (`uuid`),INDEX `uuid` (`uuid`, `balance`) );");

			stmt.executeBatch();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

    

	public void close(Connection conn, PreparedStatement ps, ResultSet res) {
        if (conn != null) try { conn.close(); } catch (SQLException ignored) {}
        if (ps != null) try { ps.close(); } catch (SQLException ignored) {}
        if (res != null) try { res.close(); } catch (SQLException ignored) {}
	}

	public Connection getConnection() throws SQLException {
		return dataSource.getConnection();
	}

	public void closePool() {
		if (dataSource != null && !dataSource.isClosed()) {
			dataSource.close();
		}
	}
}
