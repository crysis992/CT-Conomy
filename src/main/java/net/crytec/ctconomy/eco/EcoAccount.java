package net.crytec.ctconomy.eco;

import java.util.UUID;

public class EcoAccount {

	private UUID uuid;
	private double balance = 0;
	private double bank = 0;

	public EcoAccount(UUID uuid, double balance, double bank) {
		this.uuid = uuid;
		this.balance = balance;
		this.bank = bank;
	}

	public double getBalance() {
		return this.balance;
	}

	public synchronized void setBalance(double balance) {
		this.balance = balance;
	}

	public double getBank() {
		return this.bank;
	}

	public void setBank(double bank) {
		this.bank = bank;
	}

	public UUID getUUID() {
		return this.uuid;
	}

	/**
	 * Returns the players money (bank + hand)
	 * 
	 * @return
	 */
	public double getAll() {
		return (bank + balance);
	}

	/**
	 * Add the given value to the players bank.
	 * 
	 * @param amount
	 */
	public synchronized void depositBank(double amount) {
		this.bank += amount;
	}

	/**
	 * Withdraw an amount from the bank value. Cannot get into negative amount.
	 * 
	 * @param amount
	 * @return Returns true if it was successfull, false otherwise.
	 */
	public synchronized boolean withdrawBank(double amount) {
		if (this.bank >= amount) {
			this.bank -= amount;
			return true;
		}
		return false;
	}

	/**
	 * Withdraw an amount from the hand value Cannot get into negative amount.
	 * 
	 * @param amount
	 * @return Returns true if it was successfull, false otherwise.
	 */
	public synchronized boolean withdraw(double amount) {
		if (this.balance >= amount) {
			this.balance -= amount;
			return true;
		}
		return false;
	}

	/**
	 * Deposit an amount to the players hand value.
	 * 
	 * @param amount
	 */
	public synchronized void deposit(double amount) {
		this.balance += amount;
	}
}