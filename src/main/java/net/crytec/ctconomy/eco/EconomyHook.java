package net.crytec.ctconomy.eco;

import java.util.List;
import java.util.UUID;

import org.apache.commons.lang.Validate;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;

import net.crytec.ctconomy.CryConomy;
import net.crytec.ctconomy.utils.Utils;
import net.milkbowl.vault.economy.Economy;
import net.milkbowl.vault.economy.EconomyResponse;
import net.milkbowl.vault.economy.EconomyResponse.ResponseType;

public class EconomyHook implements Economy {
	private CryConomy plugin;

	public EconomyHook(CryConomy instance) {
		plugin = instance;
	}

	@Override
	public EconomyResponse bankBalance(String name) {
		return new EconomyResponse(0, 0, ResponseType.NOT_IMPLEMENTED, "This plugin does not have bank support implemented!");
	}

	@Override
	public EconomyResponse bankDeposit(String name, double amount) {
		return new EconomyResponse(0, 0, ResponseType.NOT_IMPLEMENTED, "This plugin does not have bank support implemented!");
	}

	@Override
	public EconomyResponse bankHas(String name, double amount) {
		return new EconomyResponse(0, 0, ResponseType.NOT_IMPLEMENTED, "This plugin does not have bank support implemented!");
	}

	@Override
	public EconomyResponse bankWithdraw(String name, double amount) {
		return new EconomyResponse(0, 0, ResponseType.NOT_IMPLEMENTED, "This plugin does not have bank support implemented!");
	}

	@Override
	public EconomyResponse createBank(String name, OfflinePlayer player) {
		return new EconomyResponse(0, 0, ResponseType.NOT_IMPLEMENTED, "This plugin does not have bank support implemented!");
	}

	@Override
	public EconomyResponse deleteBank(String name) {
		return new EconomyResponse(0, 0, ResponseType.NOT_IMPLEMENTED, "This plugin does not have bank support implemented!");
	}

	@Override
	public List<String> getBanks() {
		return null;
	}

	@Override
	public boolean hasBankSupport() {
		return false;
	}

	@Override
	public EconomyResponse isBankMember(String name, OfflinePlayer player) {
		return new EconomyResponse(0, 0, ResponseType.NOT_IMPLEMENTED, "This plugin does not have bank support implemented!");
	}

	@Override
	public EconomyResponse isBankOwner(String name, OfflinePlayer player) {
		return new EconomyResponse(0, 0, ResponseType.NOT_IMPLEMENTED, "This plugin does not have bank support implemented!");
	}

	@Override
	public boolean createPlayerAccount(OfflinePlayer player) {
		plugin.getAccountManager().createNewAccount(player.getUniqueId());
		return true;
	}

	@Override
	public boolean createPlayerAccount(OfflinePlayer player, String world) {
		plugin.getAccountManager().createNewAccount(player.getUniqueId());
		return true;
	}

	@Override
	public String currencyNamePlural() {
		return plugin.getConfig().getString("eco.currencyPlural");
	}

	@Override
	public String currencyNameSingular() {
		return plugin.getConfig().getString("eco.currencySingular");
	}

	@Override
	public EconomyResponse depositPlayer(OfflinePlayer player, double amount) {
		if (player == null) {
			return new EconomyResponse(0.0D, 0.0D, EconomyResponse.ResponseType.FAILURE, "This player has no economy account.");
		}

		UUID uuid = player.getUniqueId();
		plugin.getAccountManager().depositPlayer(uuid, amount);
		return new EconomyResponse(amount, plugin.getAccountManager().getAccount(uuid).getBalance(), EconomyResponse.ResponseType.SUCCESS, "");
	}

	@Override
	public EconomyResponse depositPlayer(OfflinePlayer player, String world, double amount) {
		return depositPlayer(player, amount);
	}

	@Override
	public String format(double money) {
		return String.valueOf(Utils.trim(2, money));
	}

	@Override
	public int fractionalDigits() {
		return 0;
	}

	@Override
	public double getBalance(OfflinePlayer player) {
		Validate.notNull(player, "player cannot be null");
		return plugin.getAccountManager().getAccount(player.getUniqueId()).getBalance();
	}

	@Override
	public double getBalance(OfflinePlayer player, String world) {
		return getBalance(player);
	}

	@Override
	public String getName() {
		return "CTConomy";
	}

	@Override
	public boolean has(OfflinePlayer player, double money) {
		if (plugin.getAccountManager().getAccount(player.getUniqueId()).getBalance() >= money) {
			return true;
		} else {
			return false;
		}
	}

	@Override
	public boolean has(OfflinePlayer player, String world, double money) {
		if (plugin.getAccountManager().getAccount(player.getUniqueId()).getBalance() >= money) {
			return true;
		} else {
			return false;
		}
	}

	@Override
	public boolean hasAccount(OfflinePlayer player) {
		return plugin.getAccountManager().hasAccount(player.getUniqueId());
	}

	@Override
	public boolean hasAccount(OfflinePlayer player, String world) {
		return plugin.getAccountManager().hasAccount(player.getUniqueId());
	}

	@Override
	public boolean isEnabled() {
		return true;
	}

	@Override
	public EconomyResponse withdrawPlayer(OfflinePlayer player, double amount) {
		if (player == null) {
			return new EconomyResponse(0.0D, 0.0D, EconomyResponse.ResponseType.FAILURE, "No Player found");
		}

		UUID uuid = player.getUniqueId();
		double oldAmount = getBalance(Bukkit.getOfflinePlayer(uuid));
		if (amount < 0.0D) {
			return new EconomyResponse(0.0D, oldAmount, EconomyResponse.ResponseType.FAILURE, "Money would have an negative amount.");
		}
		if (plugin.getAccountManager().withdrawPlayer(uuid, amount)) {
			return new EconomyResponse(amount, plugin.getAccountManager().getAccount(uuid).getBalance(), EconomyResponse.ResponseType.SUCCESS, "Success");
		} else {
			return new EconomyResponse(0.0D, oldAmount, ResponseType.FAILURE, "Not enough money");
		}
	}

	@Override
	public EconomyResponse withdrawPlayer(OfflinePlayer player, String world, double amount) {
		return withdrawPlayer(player, amount);
	}

	
	
	
	
	
	/*
	 * Everything below is name based access, we do not support that since names can change. Plugins should use UUIDs since years!
	 */
	
	@Override
	public EconomyResponse createBank(String name, String player) {
		return new EconomyResponse(0, 0, ResponseType.NOT_IMPLEMENTED, "Illegal Account access - Use UUID instead of player names!");
	}

	@Override
	public boolean createPlayerAccount(String playerName) {
		throw new UnsupportedOperationException("Illegal Account access - Use UUID instead of player names!");
	}

	@Override
	public boolean createPlayerAccount(String playerName, String worldName) {
		throw new UnsupportedOperationException("Illegal Account access - Use UUID instead of player names!");
	}

	@Override
	public EconomyResponse depositPlayer(String playerName, double amount) {
		return new EconomyResponse(0, 0, ResponseType.NOT_IMPLEMENTED, "Illegal Account access - Use UUID instead of player names!");
	}

	@Override
	public EconomyResponse depositPlayer(String playerName, String worldName, double amount) {
		return new EconomyResponse(0, 0, ResponseType.NOT_IMPLEMENTED, "Illegal Account access - Use UUID instead of player names!");
	}

	@Override
	public double getBalance(String playerName) {
		throw new UnsupportedOperationException("Illegal Account access - Use UUID instead of player names!");
	}

	@Override
	public double getBalance(String playerName, String world) {
		throw new UnsupportedOperationException("Illegal Account access - Use UUID instead of player names!");
	}

	@Override
	public boolean has(String playerName, double amount) {
		throw new UnsupportedOperationException("Illegal Account access - Use UUID instead of player names!");
	}

	@Override
	public boolean has(String playerName, String worldName, double amount) {
		throw new UnsupportedOperationException("Illegal Account access - Use UUID instead of player names!");
	}

	@Override
	public boolean hasAccount(String playerName) {
		throw new UnsupportedOperationException("Illegal Account access - Use UUID instead of player names!");
	}

	@Override
	public boolean hasAccount(String playerName, String worldName) {
		throw new UnsupportedOperationException("Illegal Account access - Use UUID instead of player names!");
	}

	@Override
	public EconomyResponse isBankMember(String name, String playerName) {
		return new EconomyResponse(0, 0, ResponseType.NOT_IMPLEMENTED, "Illegal Account access - Use UUID instead of player names!");
	}

	@Override
	public EconomyResponse isBankOwner(String name, String playerName) {
		return new EconomyResponse(0, 0, ResponseType.NOT_IMPLEMENTED, "Illegal Account access - Use UUID instead of player names!");
	}

	@Override
	public EconomyResponse withdrawPlayer(String playerName, double amount) {
		return new EconomyResponse(0, 0, ResponseType.NOT_IMPLEMENTED, "Illegal Account access - Use UUID instead of player names!");
	}

	@Override
	public EconomyResponse withdrawPlayer(String playerName, String worldName, double amount) {
		return new EconomyResponse(0, 0, ResponseType.NOT_IMPLEMENTED, "Illegal Account access - Use UUID instead of player names!");
	}
}