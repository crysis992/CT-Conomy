package net.crytec.ctconomy.utils;

import java.util.List;
import java.util.stream.Collectors;

import org.bukkit.ChatColor;
import org.bukkit.configuration.file.YamlConfiguration;
 
/**
* An enum for requesting strings from the language file.
*/

public enum Language {
    TITLE("title-name", "&7[&cMoney&7]&r"),
	
    ERROR_UUID("error.uuidResolve", "&4Die UUID des Spielers konnte nicht ermittelt werden. Hast du vielleicht den Namen falsch angegeben?"),
    ERROR_INVALID_NUMBER("error.invalidNumber", "&4%number% ist keine gültige Zahl."),
    ERROR_NOT_ONLINE("error.notOnline", "&4Dieser Spieler ist nicht online"),
    ERROR_SELF("error.self", "&4Du kannst dir selbst kein Geld senden"),
    ERROR_NEGATIVE("error.negative", "&4Du kannst keine leeren oder negativen Beträge senden"),
    ERROR_NOT_ENOUGH_MONEY("error.notEnoughMoney", "&4Du hast nicht genug Geld für diese Transaktion"),
    
    
    MONEY_COMMAND_PAY_SELF("money.command.pay.self", "&7Du hast %money% %currency% an %target% gesendet."),
    MONEY_COMMAND_PAY_TARGET("money.command.pay.target", "&7Du hast von %target% %money% %currency% erhalten."),
    
    MONEY_COMMAND("commands.money", "&7Du hast aktuell &e%money% %currency%&7 bei dir."),
    MONEY_COMMAND_OTHER("commands.moneyother", "&a%target%&7 hat aktuell &e%money% %currency%&7 bei sich."),
    
    MONEY_COMMAND_SET_SENDER("money.commands.setSender", "&7Du hast das Geld von %target% auf %money% %currency% gesetzt."),
    MONEY_COMMAND_SET_RECEIVER("money.commands.setReceover", "&7%target% hat dein Geld auf %money% %currency% gesetzt."),
    
    MONEY_COMMAND_GIVE_SENDER("money.commands.give", "&7Du hast %target% %money% %currency% gegeben."),
    MONEY_COMMAND_GIVE_ALL("money.commands.giveAll", "&7Du hast allen Spielern %money% gegeben."),
    
    MONEY_COMMAND_TAKE_SENDER("money.commands.take", "&7Du %money% %currency% von %target% abgezogen."),
    
    MONEY_TOP_10_HEAD("money.top10.head", "&7Top 10 Rangliste"),
    MONEY_TOP_10_ENTRY("money.top10.entry", "&e#%rank% &7Spieler &6%player% &7besitzt &e%money% %currency%&7.");
	
    private String path;
    private String def;
    private boolean isArray = false;
    
    private List<String> defArray;
    private static YamlConfiguration LANG;
 
    /**
    * Lang enum constructor.
    * @param path The string path.
    * @param start The default string.
    */
    private Language(String path, String start) {
        this.path = path;
        this.def = start;
    }
    
    private Language(String path, List<String> start) {
        this.path = path;
        this.defArray = start;
        this.isArray = true;
    }
 
    /**
    * Set the {@code YamlConfiguration} to use.
    * @param config The config to set.
    */
    public static void setFile(YamlConfiguration config) {
        LANG = config;
    }
    
    public static YamlConfiguration getFile() {
    	return LANG;
    }
 
    @Override
    public String toString() {
        if (this == TITLE) return ChatColor.translateAlternateColorCodes('&', LANG.getString(this.path, def)) + " ";
        return ChatColor.translateAlternateColorCodes('&', LANG.getString(this.path, def));
    }
    
    /**
     * Get the String with the TITLE
     * @return
     */
    public String toChatString() {
    	return TITLE.toString() + ChatColor.translateAlternateColorCodes('&', LANG.getString(this.path, def));
    }
    
    public List<String> getDescriptionArray() {
    	return LANG.getStringList(this.path).stream().map(x -> ChatColor.translateAlternateColorCodes('&', x)).collect(Collectors.toList());
    }
    
    public boolean isArray() {
    	return this.isArray;
    }
    
    public List<String> getDefArray() {
    	return this.defArray;
    }
     
    /**
    * Get the default value of the path.
    * @return The default value of the path.
    */
    public String getDefault() {
        return this.def;
    }
 
    /**
    * Get the path to the string.
    * @return The path to the string.
    */
    public String getPath() {
        return this.path;
    }
}
